import React, {useEffect, useRef} from "react";
import ChatHeader from "../ChatHeader";
import Loader from "../Loader";
import ChatTypeField from "../ChatTypeField";
import ChatMessages from "../ChatMessages";
import {
    getGroupMessageByDate,
    addNewMessage,
    controlLike,
    removeMessage,
    getLastMessageByUser, updateMessageById
} from "../../helpers/messageHelpers";
import {scrollElementToBottom} from "../../helpers";


const HEADER_HEIGHT = '86px';
const FOOTER_HEIGHT = '40px';

const Chat = ({messages, setMessages, currentUser}) => {
    const messageContainer = useRef(null);

    const saveNewMessage = (newMessage) => {
        addNewMessage({messages, setMessages, newMessage, author: currentUser})
        setTimeout(()=> scrollElementToBottom(messageContainer.current), 0)
    }



    if (messages) {
        return (
            <main className='flex flex-col justify-between flex-grow m-auto w-full h-full max-w-screen-md p-5 rounded' style={{height: `calc(100% - ${HEADER_HEIGHT} - ${FOOTER_HEIGHT})`}}>
                <ChatHeader messages={messages}/>
                <ChatMessages
                    messages={getGroupMessageByDate(messages)}
                    controlLike={(messageId) => controlLike(messages, setMessages, messageId)}
                    currentUser={currentUser} ref={messageContainer}
                    removeMessage={(id) => removeMessage(messages, setMessages, id)}
                />
                <ChatTypeField
                    saveNewMessage={saveNewMessage}
                    getLastMessage={() => getLastMessageByUser(messages, currentUser.userId)}
                    updateMessage={(messageId, newMessage) => updateMessageById({messages, setMessages, messageId, newMessage})}
                />
            </main>
        )
    }

    return <Loader />

}

export default React.memo(Chat);
