import React from "react";

const AppFooter = () => (
    <footer className="text-center font-bold p-2 bg-white shadow">
        <small>© 2005-2020 Binary-Studio, All rights reserved</small>
    </footer>
)

export default React.memo(AppFooter);
