import React, {createContext, useEffect, useState} from 'react';
import './App.css';
import AppHeader from "../AppHeader";
import AppFooter from "../AppFooter";
import Chat from "../Chat";
import {getMessage} from "../../api/message";
import {getRandomInt} from "../../helpers";


function App() {
    const [messages, setMessages] = useState(null)
    const [currentUser, setCurrentUser] = useState(null)

    useEffect(() => {
        (async function uploadMessages() {
            const messagesData = await getMessage();
            setMessages(messagesData);

            const {id, editedAt, createdAt, text, ...userData} = messagesData[getRandomInt(0, messagesData.length)];
            setCurrentUser(userData)
        })();
    }, [])

    return (
        <section className="flex flex-col justify-between h-screen bg-gray-200 ">
            <AppHeader/>
            <Chat messages={messages} setMessages={setMessages} currentUser={currentUser}/>
            <AppFooter/>
        </section>
    );
}

export default App;
