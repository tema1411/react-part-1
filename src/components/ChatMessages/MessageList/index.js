import React from "react";
import MessageItem from "./MessageItem";

const MessageList = ({dateAgo, messages, currentUser, controlLike, removeMessage}) => (
    <>
        <p className="flex justify-between items-center mb-2 mt-2">
            <span className="block flex-grow bg-gray-300" style={{height: 1}}/>
            <small className="font-bold opacity-75 ml-3 mr-3">{dateAgo}</small>
            <span className="block flex-grow bg-gray-300" style={{height: 1}}/>
        </p>
        <ul className="flex flex-col list-none">
            {messages.map((message) => <MessageItem  removeMessage={removeMessage} controlLike={controlLike} messages={message} currentUser={currentUser} key={message.id}/>)}
        </ul>
    </>
)

export default MessageList
