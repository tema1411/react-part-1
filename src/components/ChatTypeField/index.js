import React, {useState} from "react";

const ChatTypeField = ({saveNewMessage, updateMessage, getLastMessage}) => {
    const [message, setMessage] = useState('');
    const [isFocus, setIsFocus] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [idChangMessage, setIdChangMessage] = useState(false);


    const sendNewMessage = (e) => {
        e.preventDefault();
        saveNewMessage(message);
        setMessage('')
    }

    const updateOldMessage = (e) => {
        e.preventDefault();
        updateMessage(idChangMessage, message);
        setMessage('')
        setIsEdit(false)
    }

    const handlerChangeLastMessage = (e) => {
        if (isFocus && !message.length && e.key === 'ArrowUp') {
            setIsEdit(true);
            const {text, id} = getLastMessage();
            setMessage(text);
            setIdChangMessage(id);
        }
    }

    return (
        <form className="w-full bg-white shadow-lg flex p-2 h-20" onSubmit={isEdit ? updateOldMessage : sendNewMessage}>
            <textarea
                required
                className="border w-full mr-2 rounded resize-none p-2"
                placeholder="Type message"
                onChange={e => setMessage(e.target.value)} value={message}
                onFocus={() => setIsFocus(true)}
                onBlur={() => setIsFocus(false)}
                onKeyDown={handlerChangeLastMessage}
            />
            <button
                className="bg-blue-500 p-2 rounded text-white font-bold hover:bg-blue-500 focus: bg-blue-300 flex-shrink-0"
            >
                Send
            </button>
        </form>
    )
}

export default React.memo(ChatTypeField);
