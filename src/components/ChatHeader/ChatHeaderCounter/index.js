import React from "react";
const ChatHeaderCounter = ({count, countName}) => {
    return (
        <p className="m-0 mr-2">
            <b className="text-red600">{count}</b>
            <span> {countName}</span>
        </p>
    )
}

export default React.memo(ChatHeaderCounter)
