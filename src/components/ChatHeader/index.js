import React, {useEffect} from "react";
import ChatHeaderCounter from "./ChatHeaderCounter";
import * as dayjs from "dayjs";

const ChatHeader = ({messages}) => {
    const getParticipantsLength =  (messages) => {
        const participants = new Set(messages.map(({userId}) => userId));
        return participants.size
    }

    return (
        <section className="flex justify-between shadow-lg bg-white p-2 rounded">
            <div className="flex">
                <h1 className="mr-20 font-extrabold">My chat</h1>
                <ChatHeaderCounter count={getParticipantsLength(messages)} countName="participants"/>
                <ChatHeaderCounter count={messages?.length ?? 0} countName="messages"/>
            </div>
            <p>
                last message at <b>{dayjs(messages[messages.length - 1].createdAt).format('HH:mm')}</b>
            </p>
        </section>
    )
}

export default React.memo(ChatHeader)
