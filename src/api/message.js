const URL_FOR_GET_MESSAGE = 'https://edikdolynskyi.github.io/react_sources/messages.json'

export const  getMessage = async () => {
    const response = await fetch(URL_FOR_GET_MESSAGE);

    if (!response.ok) {
        return  alert('Уппс, что-то пошло не так. Сообщения не были поулчены :(')
    }

    return await response.json();
}
