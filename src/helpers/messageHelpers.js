import nextId from "react-id-generator";
import * as dayjs from "dayjs";
const isToday = require('dayjs/plugin/isToday')
const relativeTime = require('dayjs/plugin/relativeTime');
const isYesterday = require('dayjs/plugin/isYesterday')
dayjs.extend(isYesterday)
dayjs.extend(relativeTime);
dayjs.extend(isToday);

export const getNewMessageDate = (text, author) => ({
    id: nextId(),
    text,
    user: author.user,
    userId: author.userId,
    editedAt: '',
    createdAt: new Date().toISOString()
})


export const addNewMessage = ({messages, newMessage, author, setMessages}) => {
    const newMessageData = getNewMessageDate(newMessage, author)
    setMessages([...messages, newMessageData]);
}

export const removeMessage = (messages, setMessages, messageId) => {
    setMessages(messages.filter((message) => message.id !== messageId))
}

export const controlLike = (messages, setMessages, messageId ) => {
    const updatedMessagesList = messages.map((message) => {
        if (message.id === messageId) {
            message.liked = !message.liked;
        }
        return message

    });
    setMessages(updatedMessagesList);
}

export const updateMessageById = ({messages, setMessages, messageId, newMessage}) => {
    const updatedMessagesList = messages.map((message) => {
        if (message.id === messageId) {
            message.text = newMessage
        }
        return message

    });
    setMessages(updatedMessagesList);
}

export const getLastMessageByUser = (messages, userId) => messages.reverse().find((message) => message.userId === userId)

const getSortedMessageByTime = (message) => {
    return message.sort((messageA, messageB) => {
        if (dayjs(messageA.createdAt).isBefore(dayjs(messageB.createdAt))) {
            return -1;
        }
        if (dayjs(messageA.createdAt).isAfter(dayjs(messageB.createdAt))) {
            return 1;
        }
        return 0;
    })
}


export const getGroupMessageByDate = (messages) => {
    const messagesGroupList = new Map();
    const sortedMessage = getSortedMessageByTime(messages);

    sortedMessage.forEach((message) => {
        let timeMessageCreate = dayjs(message.createdAt).fromNow()

        if (dayjs(message.createdAt).isYesterday()) {
            timeMessageCreate = 'Yesterday'
        }

        if (dayjs(message.createdAt).isToday()) {
            timeMessageCreate = 'Today'
        }

        if (messagesGroupList.has(timeMessageCreate)) {
            return messagesGroupList.set(timeMessageCreate, [...messagesGroupList.get(timeMessageCreate), message])
        }

        return messagesGroupList.set(timeMessageCreate, [message])
    })
    return messagesGroupList;
}


